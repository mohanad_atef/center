<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class notification_Center  extends Model
{
    protected $fillable = [
        'notification_id','center_id'
    ];

    protected $table = 'notification_center';
    public $timestamps = true;

}