<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TempletStudent extends Model
{
    protected $fillable = [
        'name','mobile','center','year','email'
    ];
    protected $table = 'templet_studentes';
    public $timestamps = true;

}