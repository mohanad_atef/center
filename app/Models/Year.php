<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Year extends Model
{
    protected $fillable = [
        'name','order'
    ];
    public function student()
    {
        return $this->hasMany('App\Models\Student');
    }
    public function book()
    {
        return $this->hasMany('App\Models\Book');
    }
    public function video()
    {
        return $this->belongsToMany('App\Models\Video');
    }
    public function notification()
    {
        return $this->belongsToMany('App\Models\Notification');
    }
    protected $table = 'years';
    public $timestamps = true;

}