<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Video_Center  extends Model
{
    protected $fillable = [
        'video_id','center_id'
    ];

    protected $table = 'video_center';
    public $timestamps = true;

}