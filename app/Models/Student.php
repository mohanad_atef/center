<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
        'center_id','mobile','year_id','user_id'
    ];
    public function center()
    {
        return $this->belongsTo('App\Models\Center','center_id');
    }
    public function year()
    {
        return $this->belongsTo('App\Models\Year','year_id');
    }
    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
    protected $table = 'students';
    public $timestamps = true;

}