<?php

namespace App\Models;
use App\Models\Year;
use App\Models\Center;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Notification extends Model
{
    protected $fillable = [
        'order','description','year_id'
    ];
    public function center()
    {
        return $this->belongsToMany(Center::class, 'notification_center')->withTimestamps('created_at','updated_at');
    }
    public function year()
    {
        return $this->belongsTo('App\Models\Year','year_id');
    }
    protected $table = 'notifications';
    public $timestamps = true;

}