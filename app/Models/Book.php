<?php

namespace App\Models;
use App\Models\Year;
use App\Models\Center;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Book extends Model
{
    protected $fillable = [
        'title','order','description','pdf','year_id'
    ];
    public function center()
    {
        return $this->belongsToMany(Center::class, 'book_center')->withTimestamps('created_at','updated_at');
    }
    public function year()
    {
        return $this->belongsTo('App\Models\Year','year_id');
    }
    protected $table = 'books';
    public $timestamps = true;

}