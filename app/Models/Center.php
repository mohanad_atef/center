<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Center extends Model
{
    protected $fillable = [
        'name','order'
    ];
    public function student()
    {
        return $this->hasMany('App\Models\Student');
    }
    public function book()
    {
        return $this->belongsToMany('App\Models\Book', 'Book_Center', 'center_id','book_id')->paginate();
    }
    public function video()
    {
        return $this->belongsToMany('App\Models\Video', 'Video_Center', 'center_id','book_id')->paginate();
    }
    protected $table = 'centers';
    public $timestamps = true;

}