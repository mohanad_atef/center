<?php

namespace App\Models;
use App\Models\Year;
use App\Models\Center;
use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $fillable = [
        'title','order','description','video_url','year_id'
    ];
    public function center()
    {
        return $this->belongsToMany(Center::class, 'video_center')->withTimestamps('created_at','updated_at');
    }
    public function year()
    {
        return $this->belongsTo('App\Models\Year','year_id');
    }
    protected $table = 'videos';
    public $timestamps = true;

}