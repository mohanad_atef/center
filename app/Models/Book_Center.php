<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Book_Center  extends Model
{
    protected $fillable = [
        'book_id','center_id'
    ];

    protected $table = 'book_center';
    public $timestamps = true;

}