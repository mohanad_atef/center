<?php

namespace App\Http\Requests\admin\Notification;

use Illuminate\Foundation\Http\FormRequest;

class NotificationEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

            return [
                'description'=>'required',
                'order'=>'required',
                'year_id' => 'required',
                'center' => 'required|exists:centers,id',
            ];

    }
}
