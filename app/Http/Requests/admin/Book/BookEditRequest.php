<?php

namespace App\Http\Requests\admin\Book;

use Illuminate\Foundation\Http\FormRequest;

class BookEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->pdf == null)
        {
            return [
                'title'=>'required',
                'description'=>'required',
                'order'=>'required',
                'year_id' => 'required',
                'center' => 'required|exists:centers,id',
            ];
        }
        else
        {
            return [
                'title'=>'required',
                'description'=>'required',
                'order'=>'required',
                'year' => 'required|exists:years,id',
                'center' => 'required|exists:centers,id',
                'pdf'=>'required|file|mimes:pdf',
            ];
        }
    }
}
