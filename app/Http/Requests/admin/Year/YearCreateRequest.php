<?php

namespace App\Http\Requests\admin\Year;

use Illuminate\Foundation\Http\FormRequest;

class YearCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|unique:years',
            'order'=>'required|min:1',
        ];
    }
}
