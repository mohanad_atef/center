<?php

namespace App\Http\Requests\admin\Teacher;

use Illuminate\Foundation\Http\FormRequest;

class TeacherEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mobile'=>'required|min:1',
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
        ];
    }
}
