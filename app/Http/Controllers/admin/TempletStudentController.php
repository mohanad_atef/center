<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\admin\import\ImportCreateRequest;
use App\Imports\TempletStudentImport;
use App\Models\Center;
use App\Models\Student;
use App\Models\TempletStudent;
use App\Models\Year;
use App\Role_user;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;


class TempletStudentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function indextempletstudent()
    {
        $templet_student = TempletStudent::all();
        return view('admin.import.import_index', compact('templet_student'));
    }

    public function importExportView()
    {
        ini_set('max_execution_time', 120000);
        ini_set('post_max_size', 120000);
        ini_set('upload_max_filesize', 100000);
        return view('/admin/import');
    }

    public function importget()
    {
        return view('admin.import.import');
    }

    public function importpost(ImportCreateRequest $request)
    {
        ini_set('max_execution_time', 120000);
        ini_set('post_max_size', 120000);
        ini_set('upload_max_filesize', 100000);
        TempletStudent::truncate();
        Excel::import(new TempletStudentImport(), request()->file('file'));
        $templet_student = TempletStudent::find(1);
        $templet_student->delete();
        return redirect('/admin/import/index')->with('message', 'Add Templet Is Done!');
    }

    public function unmatchedStudentGrouped()
    {
        ini_set('max_execution_time', 120000);
        ini_set('post_max_size', 120000);
        ini_set('upload_max_filesize', 100000);
        $matched_result = DB::table("centers")->rightjoin('templet_studentes', 'centers.name', '=', 'templet_studentes.center')
            ->where('centers.name', '=', null)
            ->select('templet_studentes.center')
            ->groupBy('templet_studentes.center')
            ->get();
        $center = [];
        for ($i = 0; $i < count($matched_result); $i++) {
            array_push($center, $matched_result[$i]->center);
        }
        $center = array_diff($center, ['', 'null']);
        $matched_result = DB::table("years")->rightjoin('templet_studentes', 'years.name', '=', 'templet_studentes.year')
            ->where('years.name', '=', null)
            ->select('templet_studentes.year')
            ->groupBy('templet_studentes.year')
            ->get();
        $year = [];
        for ($i = 0; $i < count($matched_result); $i++) {
            array_push($year, $matched_result[$i]->year);
        }
        $year = array_diff($year, ['', 'null']);
        $matched_result = DB::table("users")->rightjoin('templet_studentes', 'users.email', '=', 'templet_studentes.email')
            ->where('users.email', '!=', null)
            ->select('templet_studentes.email')
            ->groupBy('templet_studentes.email')
            ->get();
        $email = [];
        for ($i = 0; $i < count($matched_result); $i++) {
            array_push($email, $matched_result[$i]->email);
        }
        $email = array_diff($email, ['', 'null']);
        $matched_result = DB::table("users")->rightjoin('templet_studentes', 'users.email', '=', 'templet_studentes.email')
            ->where('users.email', '=', null)
            ->select('templet_studentes.email')
            ->get();
        $email1 = [];
        for ($i = 0; $i < count($matched_result); $i++) {
            array_push($email1, $matched_result[$i]->email);
        }
        $email1 = array_diff($email1, ['', 'null']);
        $matched_result = DB::table("users")->rightjoin('templet_studentes', 'users.email', '=', 'templet_studentes.email')
            ->where('users.email', '!=', null)
            ->select('templet_studentes.email')
            ->get();
        $email11 = [];
        for ($i = 0; $i < count($matched_result); $i++) {
            array_push($email11, $matched_result[$i]->email);
        }
        $email11 = array_diff($email11, ['', 'null']);
        $emailtemplet1 = [];
        $emailtemplet11 = [];
        for ($i=0;$i<count($email1);$i++)
        {
            for ($s=0;$s<count($email1);$s++)
            {
                if ($email1[$i] == $email1[$s] && $i != $s)
                {
                    array_push($emailtemplet1, $email1[$s]);
                }
            }
        }
        $emailtemplet1=array_unique($emailtemplet1);
        for ($i=0;$i<count($email11);$i++)
        {
            for ($s=0;$s<count($email11);$s++)
            {
                if ($email11[$i] == $email11[$s] && $i != $s)
                {
                    array_push($emailtemplet11, $email11[$s]);
                }
            }
        }
        $emailtemplet11=array_unique($emailtemplet11);
        $count_error = count($center + $year + $email + $emailtemplet1 +$emailtemplet11);
        if ($count_error > 0) {
            return view('admin.import.error', compact('center', 'year', 'email', 'emailtemplet1','emailtemplet11'));
        } else {
            return view('admin.import.save');
        }
    }

    public function SaveStudentGrouped()
    {
        ini_set('max_execution_time', 120000);
        ini_set('post_max_size', 120000);
        ini_set('upload_max_filesize', 100000);
        $templet_student = TempletStudent::all();
        foreach ($templet_student as $templet) {
            $newuser = new User();
            $newuser->name = $templet->name;
            $newuser->email = $templet->email;
            $newuser->password = Hash::make(123456);
            $newuser->statues = 1;
            $newuser->save();
            $user = User::all()->last();
            $id = $user->id;
            $newroleuser = new Role_user();
            $newroleuser->user_id = $id;
            $newroleuser->role_id = 2;
            $newroleuser->save();
            $newstudent = new Student();
            $newstudent->user_id = $id;
            $center = Center::where('name', $templet->center)->first();
            $newstudent->center_id = $center->id;
            $year = Year::where('name', $templet->year)->first();
            $newstudent->year_id = $year->id;
            $newstudent->mobile = $templet->mobile;
            $newstudent->save();
        }
        return redirect('admin/student')->with('message', 'Add Medical Place Is Done!');
    }
}

?>