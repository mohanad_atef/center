<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\admin\Year\YearEditRequest;
use App\Http\Requests\admin\Year\YearCreateRequest;
use App\Models\Year;


class YearController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function indexyear()
    {
        $year = Year::orderBy('order')->get();
        return view('admin.year.year_index', compact('year'));
    }

    public function createyearpost(YearCreateRequest $request)
    {
        $newyear = new Year();
        $newyear->name = $request->input('name');
        $newyear->order = $request->input('order');
        $newyear->save();
        return redirect('/admin/year')->with('message', 'Add Year Is Done!');
    }

    public function createyearget()
    {
        return view('admin.year.year_create');
    }

    public function edityearpost(YearEditRequest $request, $id)
    {
        $newyear = year::find($id);
        $newyear->name = $request->input('name');
        $newyear->order = $request->input('order');
        $newyear->save();
        return redirect('/admin/year')->with('message', 'Edit Year Is Done!');
    }

    public function edityearget($id)
    {
        $year = Year::find($id);
        return view('admin.year.year_edit', compact('year'));
    }
}

?>