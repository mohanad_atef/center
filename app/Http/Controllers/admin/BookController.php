<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\admin\Book\BookCreateRequest;
use App\Http\Requests\admin\Book\BookEditRequest;
use App\Models\Student;
use App\Models\Year;
use App\Models\Center;
use App\Models\Book_Center;
use App\Models\Book;
use App\Role_user;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class BookController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function indexbook()
    {
        $role=Role_user::where('user_id','=',Auth::User()->id)->first();
        $student=Student::where('user_id','=',Auth::User()->id)->first();
        if($role->role_id =='2')
            {
            $book = Book::where('year_id',$student->year_id)->get();
        }
        else
            {
        $book = Book::all();
        }
        return view('admin.book.book_index', compact('book','student','role'));
    }

    public function createbookpost(BookCreateRequest $request)
    {
        $newbook = new Book();
        $newbook->title = $request->input('title');
        $newbook->description = $request->input('description');
        $newbook->order = $request->input('order');
        $newbook->year_id = $request->input('year_id');
        $filename = $request->input('title').'.' . Request()->pdf->getClientOriginalExtension();
        Request()->pdf->move(public_path('files/pdf'), $filename);
        $newbook->pdf = ($filename);
        $newbook->save();
        $newbook->center()->sync((array)$request->input('center'));
        $newbook->save();
        return redirect('/admin/book')->with('message', 'Add Books Is Done!');
    }

    public function createbookget()
    {
        $year = DB::table("years")->pluck("name", "id");
        $center = Center::select('name', 'id')->get();
        return view('admin.book.book_create', compact('year','center'));
    }

    public function editbookpost(BookEditRequest $request, $id)
    {
        $newbook = Book::find($id);
        $newbook->title = $request->input('title');
        $newbook->description = $request->input('description');
        $newbook->order = $request->input('order');
        $newbook->year_id = $request->input('year_id');
        $newbook->center()->sync((array)$request->input('center'));
        if ($request->pdf != null)
        {
            $filename = Request()->pdf->getClientOriginalExtension();
            Request()->pdf->move(public_path('files/pdf'), $filename);
            $newbook->pdf = ($filename);
        }
        $newbook->save();
        return redirect('/admin/book')->with('message', 'Edit Books Is Done!');
    }

    public function editbookget($id)
    {
        $book = Book::find($id);
        $year = DB::table("years")->pluck("name", "id");
        $center = Center::select('name', 'id')->get();
        $book_center = Book_Center::all()->where('book_id','=',$id);
        return view('admin.book.book_edit', compact('book', 'year','center','book_center'));
    }

    public function deletebook($id)
    {
        $book = Book::find($id);
        $book->delete();
        return redirect()->back()->with('message', 'Delete Books Is Done!');
    }
}

?>