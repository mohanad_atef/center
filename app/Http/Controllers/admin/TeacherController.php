<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\admin\Teacher\TeacherCreateRequest;
use App\Http\Requests\admin\Teacher\TeacherEditRequest;
use App\Http\Requests\User\UserResetPasswordCreateRequest;
use App\Models\Teacher;
use App\Role_user;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class TeacherController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }
    public function indexteacher()
    {
        $teacher = Teacher::all();
        return view('admin.teacher.teacher_index', compact('teacher'));
    }

    public function createteacherpost(TeacherCreateRequest $request)
    {
        $newuser = new User();
        $newuser->name = $request->input('name');
        $newuser->email = $request->input('email');
        $newuser->password = Hash::make($request->input('password'));
        $newuser->statues = 1;
        $newuser->save();
        $user = User::all()->last();
        $id = $user->id;
        $newroleuser = new Role_user();
        $newroleuser->user_id = $id;
        $newroleuser->role_id = 3;
        $newroleuser->save();
        $newteacher = new Teacher();
        $newteacher->user_id = $id;
        $newteacher->mobile = $request->input('mobile');
        $newteacher->save();
        return redirect('admin/teacher')->with('message', 'Add Teacher Is Done!');
    }

    public function createteacherget()
    {
        return view('admin.teacher.teacher_create');
    }

    public function editteacherpost(TeacherEditRequest $request, $id)
    {
        $newuser = User::find($id);
        $newuser->name = $request->input('name');
        $newuser->email = $request->input('email');
        $newuser->save();
        $newteacher = Teacher::where('user_id','=',$id)->first();
        $newteacher->mobile = $request->input('mobile');
        $newteacher->save();
        return redirect('admin/teacher')->with('message', 'Edit Teacher Is Done!');
    }

    public function editteacherget($id)
    {
        $teacher = Teacher::with('user')->where('user_id','=',$id)->first();
        $center = DB::table("centers")->pluck("name", "id");
        $year = DB::table("years")->pluck("name", "id");
        return view('admin.teacher.teacher_edit', compact('center', 'teacher','year'));
    }

    public function resetpassworduserpost(UserResetPasswordCreateRequest $request, $id)
    {
        $newuser = User::find($id);
        $newuser->password = Hash::make($request->input('password'));
        $newuser->save();
        return redirect('admin/teacher')->with('message', 'Reset Password Teacher Is Done!');
    }

    public function resetpassworduserget($id)
    {
        $user = User::find($id);
        return view('admin.teacher.teacher_reset_password', compact('user'));
    }

    public function editstatues($id)
    {
        $newuser = User::find($id);
        if ($newuser->statues == 1) {
            $newuser->statues = '0';
        } elseif ($newuser->statues == 0) {
            $newuser->statues = '1';
        }
        $newuser->save();
        return redirect('/admin/teacher')->with('message', 'Edit Statues Is Done!');
    }


}

?>