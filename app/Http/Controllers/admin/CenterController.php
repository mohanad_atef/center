<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\admin\Center\CenterEditRequest;
use App\Http\Requests\admin\Center\CenterCreateRequest;
use App\Models\Center;


class CenterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function indexcenter()
    {
        $center = Center::orderBy('order')->get();
        return view('admin.center.center_index', compact('center'));
    }

    public function createcenterpost(CenterCreateRequest $request)
    {
        $newcenter = new Center();
        $newcenter->name = $request->input('name');
        $newcenter->order = $request->input('order');
        $newcenter->save();
        return redirect('/admin/center')->with('message', 'Add Center Is Done!');
    }

    public function createcenterget()
    {
        return view('admin.center.center_create');
    }

    public function editcenterpost(CenterEditRequest $request, $id)
    {
        $newcenter = center::find($id);
        $newcenter->name = $request->input('name');
        $newcenter->order = $request->input('order');
        $newcenter->save();
        return redirect('/admin/center')->with('message', 'Edit Center Is Done!');
    }

    public function editcenterget($id)
    {
        $center = Center::find($id);
        return view('admin.center.center_edit', compact('center'));
    }

}

?>