<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\admin\Student\StudentCreateRequest;
use App\Http\Requests\admin\Student\StudentEditRequest;
use App\Http\Requests\User\UserResetPasswordCreateRequest;
use App\Models\Student;
use App\Role_user;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class StudentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }
    public function indexstudent()
    {
        $student = Student::all();
        return view('admin.student.student_index', compact('student'));
    }

    public function createstudentpost(StudentCreateRequest $request)
    {
        $newuser = new User();
        $newuser->name = $request->input('name');
        $newuser->email = $request->input('email');
        $newuser->password = Hash::make($request->input('password'));
        $newuser->statues = 1;
        $newuser->save();
        $user = User::all()->last();
        $id = $user->id;
        $newroleuser = new Role_user();
        $newroleuser->user_id = $id;
        $newroleuser->role_id = 2;
        $newroleuser->save();
        $newstudent = new Student();
        $newstudent->user_id = $id;
        $newstudent->center_id = $request->input('center_id');
        $newstudent->year_id = $request->input('year_id');
        $newstudent->mobile = $request->input('mobile');
        $newstudent->save();
        return redirect('admin/student')->with('message', 'Add Student Is Done!');
    }

    public function createstudentget()
    {
        $center = DB::table("centers")->pluck("name", "id");
        $year = DB::table("years")->pluck("name", "id");
        return view('admin.student.student_create', compact('center','year'));
    }

    public function editstudentpost(StudentEditRequest $request, $id)
    {
        $newuser = User::find($id);
        $newuser->name = $request->input('name');
        $newuser->email = $request->input('email');
        $newuser->save();
        $newstudent = Student::where('user_id','=',$id)->first();
        $newstudent->center_id = $request->input('center_id');
        $newstudent->year_id = $request->input('year_id');
        $newstudent->mobile = $request->input('mobile');
        $newstudent->save();
        return redirect('admin/student')->with('message', 'Edit Student Is Done!');
    }

    public function editstudentget($id)
    {
        $student = Student::with('user')->where('user_id','=',$id)->first();
        $center = DB::table("centers")->pluck("name", "id");
        $year = DB::table("years")->pluck("name", "id");
        return view('admin.student.student_edit', compact('center', 'student','year'));
    }

    public function resetpassworduserpost(UserResetPasswordCreateRequest $request, $id)
    {
        $newuser = User::find($id);
        $newuser->password = Hash::make($request->input('password'));
        $newuser->save();
        return redirect('admin/student')->with('message', 'Reset Password Student Is Done!');
    }

    public function resetpassworduserget($id)
    {
        $user = User::find($id);
        return view('admin.student.student_reset_password', compact('user'));
    }

    public function editstatues($id)
    {
        $newuser = User::find($id);
        if ($newuser->statues == 1) {
            $newuser->statues = '0';
        } elseif ($newuser->statues == 0) {
            $newuser->statues = '1';
        }
        $newuser->save();
        return redirect('/admin/student')->with('message', 'Edit Statues Is Done!');
    }


}

?>