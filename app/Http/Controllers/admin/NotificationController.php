<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\admin\Notification\NotificationCreateRequest;
use App\Http\Requests\admin\Notification\NotificationEditRequest;
use App\Models\Student;
use App\Models\Year;
use App\Models\Center;
use App\Models\Notification_Center;
use App\Models\Notification;
use App\Role_user;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class NotificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function indexnotification()
    {

        $notification = Notification::all();
        return view('admin.notification.notification_index', compact('notification'));
    }

    public function createnotificationpost(NotificationCreateRequest $request)
    {
        $newnotification = new Notification();
        $newnotification->description = $request->input('description');
        $newnotification->order = $request->input('order');
        $newnotification->year_id = $request->input('year_id');
        $newnotification->save();
        $newnotification->center()->sync((array)$request->input('center'));
        $newnotification->save();
        return redirect('/admin/notification')->with('message', 'Add Notifications Is Done!');
    }

    public function createnotificationget()
    {
        $year = DB::table("years")->pluck("name", "id");
        $center = Center::select('name', 'id')->get();
        return view('admin.notification.notification_create', compact('year','center'));
    }

    public function editnotificationpost(NotificationEditRequest $request, $id)
    {
        $newnotification = Notification::find($id);
        $newnotification->description = $request->input('description');
        $newnotification->order = $request->input('order');
        $newnotification->year_id = $request->input('year_id');
        $newnotification->center()->sync((array)$request->input('center'));
        $newnotification->save();
        return redirect('/admin/notification')->with('message', 'Edit Notifications Is Done!');
    }

    public function editnotificationget($id)
    {
        $notification = Notification::find($id);
        $year = DB::table("years")->pluck("name", "id");
        $center = Center::select('name', 'id')->get();
        $notification_center = Notification_Center::all()->where('notification_id','=',$id);
        return view('admin.notification.notification_edit', compact('notification', 'year','center','notification_center'));
    }

    public function deletenotification($id)
    {
        $notification = Notification::find($id);
        $notification->delete();
        return redirect()->back()->with('message', 'Delete Notifications Is Done!');
    }
}

?>