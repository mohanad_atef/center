<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\admin\Video\VideoCreateRequest;
use App\Http\Requests\admin\Video\VideoEditRequest;
use App\Models\Student;
use App\Models\Year;
use App\Models\Center;
use App\Models\Video_Center;
use App\Models\Video;
use App\Role_user;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class VideoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function indexvideo()
    {
        $role=Role_user::where('user_id','=',Auth::User()->id)->first();
        $student=Student::where('user_id','=',Auth::User()->id)->first();
        if($role->role_id =='2')
        {
            $video = Video::where('year_id',$student->year_id)->get();
        }
        else
        {
            $video = Video::all();
        }
        return view('admin.video.video_index', compact('video','student','role'));
    }

    public function createvideopost(VideoCreateRequest $request)
    {
        $newvideo = new Video();
        $newvideo->title = $request->input('title');
        $newvideo->description = $request->input('description');
        $newvideo->order = $request->input('order');
        $newvideo->video_url = $request->input('video_url');
        $newvideo->save();
        $newvideo->year_id = $request->input('year_id');
        $newvideo->center()->sync((array)$request->input('center'));
        $newvideo->save();
        return redirect('/admin/video')->with('message', 'Add Videos Is Done!');
    }

    public function createvideoget()
    {
        $year = DB::table("years")->pluck("name", "id");
        $center = Center::select('name', 'id')->get();
        return view('admin.video.video_create', compact('year','center'));
    }

    public function editvideopost(VideoEditRequest $request, $id)
    {
        $newvideo = Video::find($id);
        $newvideo->title = $request->input('title');
        $newvideo->description = $request->input('description');
        $newvideo->order = $request->input('order');
        $newvideo->year_id = $request->input('year_id');
        $newvideo->center()->sync((array)$request->input('center'));
        $newvideo->video_url = $request->input('video_url');
        $newvideo->save();
        return redirect('/admin/video')->with('message', 'Edit Videos Is Done!');
    }

    public function editvideoget($id)
    {
        $video = Video::find($id);
        $year = DB::table("years")->pluck("name", "id");
        $center = Center::select('name', 'id')->get();
        $video_center = Video_Center::all()->where('video_id','=',$id);
        return view('admin.video.video_edit', compact('video', 'year','center','video_center'));
    }

    public function deletevideo($id)
    {
        $video = Video::find($id);
        $video->delete();
        return redirect()->back()->with('message', 'Delete Videos Is Done!');
    }

    public function showvideo($id)
    {
        $video = Video::find($id);
        return view('admin.video.video_show', compact('video'));
    }
}

?>