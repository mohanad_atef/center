<?php

namespace App\Providers;
use App\Models\Notification;
use App\Models\Student;
use App\Role_user;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(['includes.admin.header-contect'], function ($view) {
            $role=Role_user::where('user_id','=',Auth::User()->id)->first();
            $student=Student::where('user_id','=',Auth::User()->id)->first();
            if($role->role_id =='2')
            {
                $notification = Notification::where('year_id', $student->year_id)->get();
            }
            else
            {
                $notification = Notification::all();
            }
            $view->with('notification',$notification);
            $view->with('role',$role);
            $view->with('student',$student);
        });
    }
}
