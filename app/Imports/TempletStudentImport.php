<?php

namespace App\Imports;

use App\Models\TempletStudent;
use Maatwebsite\Excel\Concerns\ToModel;

class TempletStudentImport implements ToModel
{
    public function model(array $row)
    {
        return new TempletStudent([
            'name'       => $row[0],
            'email'      => $row[1],
            'mobile'       => $row[2],
            'center'      => $row[3],
            'year'     => $row[4],
        ]);
    }
    public function headings(): array
    {
        return ['name','email','mobile','center','year'];
    }
}
