<?php

namespace App\Exports;

use App\Models\TempletStudent;
use Maatwebsite\Excel\Concerns\FromCollection;

class TempletStudentExport implements FromCollection
{
    public function collection()
    {
        return  TempletStudent::all(['name','email','mobile','center','year']);
    }
    public function headings(): array
    {
        return ['name','email','mobile','center','year'];
    }
}
