<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Left side column. contains the logo and sidebar -->
        <ul class="sidebar-menu" data-widget="tree">
            @permission('dashboard-show')
            <li><a href="{{ url('/admin') }}"><i class="fa fa-th"></i> <span>الرئيسية</span></a></li>
            @endpermission
            @permission('core-data-list')
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>البيانات الرئيسية</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-right pull-left"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    @permission('center-show')
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-circle-o"></i> <span>السنتر</span>
                            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-left"></i>
            </span>
                        </a>
                        <ul class="treeview-menu">
                            @permission('center-show')
                            <li><a href="{{ url('admin/center') }}"><i class="fa fa-circle-o"></i>
                                    <span>السنتر</span></a></li>
                            @endpermission
                            @permission('center-create')
                            <li><a href="{{ url('admin/center/create') }}"><i class="fa fa-circle-o"></i>
                                    <span>اضافة سنتر</span></a></li>
                            @endpermission
                        </ul>
                    </li>
                    @endpermission
                    @permission('year-show')
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-circle-o"></i> <span>سنة</span>
                            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-left"></i>
            </span>
                        </a>
                        <ul class="treeview-menu">
                            @permission('year-show')
                            <li><a href="{{ url('admin/year') }}"><i class="fa fa-circle-o"></i>
                                    <span>سنة</span></a></li>
                            @endpermission
                            @permission('year-create')
                            <li><a href="{{ url('admin/year/create') }}"><i class="fa fa-circle-o"></i>
                                    <span>اضافة سنة</span></a></li>
                            @endpermission
                        </ul>
                    </li>
                    @endpermission
                </ul>
            </li>
            @endpermission
            @permission('notification-list')
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-paper-plane"></i> <span>اشعارات</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-right pull-left"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    @permission('notification-show')
                    <li><a href="{{ url('admin/notification')}}"><i class="fa fa-circle-o"></i>جميع الاشعارات</a></li>
                    @endpermission
                    @permission('notification-create')
                    <li><a href="{{ url('admin/notification/create') }}"><i class="fa fa-circle-o"></i>اضافة اشعار</a></li>
                    @endpermission
                </ul>
            </li>
            @endpermission
            @permission('book-list')
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-book"></i> <span>الكتب</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-right pull-left"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    @permission('book-show')
                    <li><a href="{{ url('admin/book')}}"><i class="fa fa-circle-o"></i>كل الكتب</a></li>
                    @endpermission
                    @permission('book-create')
                    <li><a href="{{ url('admin/book/create') }}"><i class="fa fa-circle-o"></i>اضافة كتاب</a></li>
                    @endpermission
                </ul>
            </li>
            @endpermission
            @permission('video-list')
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-viadeo"></i> <span>فيديو</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-right pull-left"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    @permission('video-show')
                    <li><a href="{{ url('admin/video')}}"><i class="fa fa-circle-o"></i>جميع الفيديوهات</a></li>
                    @endpermission
                    @permission('video-create')
                    <li><a href="{{ url('admin/video/create') }}"><i class="fa fa-circle-o"></i>اضافة فيديو</a></li>
                    @endpermission
                </ul>
            </li>
            @endpermission
            @permission('user-list')
            <li class="treeview">
                <a href="#"><i class="fa fa-group"></i> <span>المستخدمين</span><span class="pull-right-container"><i
                                class="fa fa-angle-right pull-left"></i></span></a>
                <ul class="treeview-menu">
                    @permission('user-show')
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-circle-o"></i> <span>User</span>
                            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-left"></i>
            </span>
                        </a>
                        <ul class="treeview-menu">
                            @permission('user-show')
                            <li><a href="{{ url('/admin/user') }}"><i class="fa fa-group"></i><span>all User</span></a>
                            </li>
                            @endpermission
                            @permission('user-create')
                            <li><a href="{{ url('/admin/user/create') }}"><i
                                            class="fa fa-group"></i><span>Add User</span></a></li>
                            @endpermission
                        </ul>
                    </li>
                    @endpermission
                    @permission('role-show')
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-circle-o"></i> <span>Role</span>
                            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-left"></i>
            </span>
                        </a>
                        <ul class="treeview-menu">
                            @permission('role-show')
                            <li><a href="{{ url('/admin/role') }}"><i class="fa fa-at"></i><span>all Role</span></a>
                            </li>
                            @endpermission
                            @permission('role-create')
                            <li><a href="{{ url('/admin/role/create') }}"><i class="fa fa-at"></i><span>Add Role</span></a>
                            </li>
                            @endpermission
                        </ul>
                    </li>
                    @endpermission
                    @permission('permission-show')
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-circle-o"></i> <span>Permission</span>
                            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-left"></i>
            </span>
                        </a>
                        <ul class="treeview-menu">
                            @permission('permission-show')
                            <li><a href="{{ url('/admin/permission') }}"><i class="fa fa-group"></i><span>all Permission</span></a>
                            </li>
                            @endpermission
                            @permission('permission-create')
                            <li><a href="{{ url('/admin/permission/create') }}"><i
                                            class="fa fa-group"></i><span>Add Permission</span></a></li>
                            @endpermission
                        </ul>
                    </li>
                    @endpermission
                    @permission('student-list')
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-user"></i> <span>الطلاب</span>
                            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-left"></i>
            </span>
                        </a>
                        <ul class="treeview-menu">
                            @permission('student-show')
                            <li><a href="{{ url('admin/student')}}"><i class="fa fa-circle-o"></i>جميع الطلاب</a></li>
                            @endpermission
                            @permission('student-create')
                            <li><a href="{{ url('admin/student/create') }}"><i class="fa fa-circle-o"></i>اضافة طالب</a></li>
                            @endpermission
                        </ul>
                    </li>
                    @endpermission
                    @permission('teacher-list')
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-user"></i> <span>المدرسين</span>
                            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-left"></i>
            </span>
                        </a>
                        <ul class="treeview-menu">
                            @permission('teacher-show')
                            <li><a href="{{ url('admin/teacher')}}"><i class="fa fa-circle-o"></i>جميع المدرسين</a></li>
                            @endpermission
                            @permission('teacher-create')
                            <li><a href="{{ url('admin/teacher/create') }}"><i class="fa fa-circle-o"></i>اضافة مدرس</a></li>
                            @endpermission
                        </ul>
                    </li>
                    @endpermission
                    @permission('import-student-create')
                    <li><a href="{{ url('/admin/import') }}"><i class="fa fa-th"></i> <span>رفع بيانات الطلبة</span></a></li>
                    @endpermission
                </ul>
            </li>
            @endpermission
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
@yield('main-sidebar')
