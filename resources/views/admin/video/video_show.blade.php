<!DOCTYPE html>
<html>
<head>
    @include('includes.admin.header')
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    @include('includes.admin.main-header')
    @include('includes.admin.main-sidebar')
    <div class="content-wrapper">
        @include('includes.admin.error')
        <div align="center">{{ __('Video') }}</div>
        <div align="center">
        <iframe width="80%" height="400"  src="//www.youtube.com/embed/{{ $video->video_url }}?autoplay=1&playlist={{ $video->video_url }}&loop=1"
                frameborder="0" class="responsive"  allowfullscreen></iframe>
    </div>
    </div>
    @include('includes.admin.footer')
    @include('includes.admin.scripts')
</div>
</body>
</html>