<!DOCTYPE html>
<html>
<head>
    @include('includes.admin.header')
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    @include('includes.admin.main-header')
    @include('includes.admin.main-sidebar')
    <div class="content-wrapper">
        <br>
        @include('includes.admin.error')
        <div class="page-content">
            <div class="page-header">
                <div class="row">
                    <div class="col-md-11">
                        <h1 align="center">Video</h1>
                    </div>
                    @permission('video-create')

                        <div style="margin-left:35px;">
                        <a href="{{  url('/admin/video/create') }}"style="font-size: 16px" class="btn btn-sm btn-primary">Add</a>
                    </div>
                    @endpermission
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    @if(count($video) > 0)
                        <div align="center" class="col-md-12 table-responsive">
                            <table id="dynamic-table" class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th class="center">#</th>
                                    <th class="center">title</th>
                                    <th class="center">description</th>
                                    @permission('control')
                                    <th class="center">center</th>
                                    <th class="center">year</th>
                                    <th class="center">Control</th>
                                    @endpermission
                                    <th class="center">show</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $count=1; ?>
                                @foreach($video as $myvideo)
                                    @if($role->role_id =='2')
                                        @foreach($myvideo->center as $video_center)
                                            @if($video_center->id == $student->center_id)
                                    <tr>
                                        <td>{{ $count++ }}</td>
                                        <td class="center">{{ $myvideo->title }}</td>
                                        <td class="center">{!! $myvideo->description !!}</td>
                                        @permission('control')
                                        <td class="center">
                                            @foreach($myvideo->center as $video_center)
                                                [{{ $video_center->name }}],
                                            @endforeach</td>
                                        <td class="center">
                                            {{ $myvideo->year->name }}
                                        </td>
                                        <td class="center">
                                            @permission('video-edit')
                                            <a href="{{ url('/admin/video/edit/'.$myvideo->id)}}"><i class="ace-icon fa fa-edit bigger-120  edit" data-id="">edit</i></a>
                                            @endpermission
                                            @permission('video-delete')
                                            <a href="{{url('admin/video/delete/'.$myvideo->id)}}" onclick="return confirm('Are you sure?')" style="color: red"> <i class="ace-icon fa fa-trash bigger-120 ">delete</i></a>
                                            @endpermission
                                        </td>
                                        @endpermission
                                        <td class="center">
                                            @permission('video-show')
                                            <a href="{{ url('/admin/video/show/'.$myvideo->id)}}"><i class="ace-icon fa fa-eye bigger-120  edit" data-id="">  show</i></a>
                                            @endpermission
                                        </td>
                                    </tr>
                                            @endif
                                        @endforeach
                                    @else
                                        <tr>
                                            <td>{{ $count++ }}</td>
                                            <td class="center">{{ $myvideo->title }}</td>
                                            <td class="center">{!! $myvideo->description !!}</td>
                                            @permission('control')
                                            <td class="center">
                                                @foreach($myvideo->center as $video_center)
                                                    [{{ $video_center->name }}],
                                                @endforeach</td>
                                            <td class="center">
                                                {{ $myvideo->year->name }}
                                            </td>
                                            <td class="center">
                                                @permission('video-edit')
                                                <a href="{{ url('/admin/video/edit/'.$myvideo->id)}}"><i class="ace-icon fa fa-edit bigger-120  edit" data-id="">edit</i></a>
                                                @endpermission
                                                @permission('video-delete')
                                                <a href="{{url('admin/video/delete/'.$myvideo->id)}}" onclick="return confirm('Are you sure?')" style="color: red"> <i class="ace-icon fa fa-trash bigger-120 ">delete</i></a>
                                                @endpermission
                                            </td>
                                            @endpermission
                                            <td class="center">
                                                @permission('video-show')
                                                <a href="{{ url('/admin/video/show/'.$myvideo->id)}}"><i class="ace-icon fa fa-eye bigger-120  edit" data-id=""> show</i></a>
                                                @endpermission
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="empty" align="center">There is no Video to show</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @include('includes.admin.footer')
    @include('includes.admin.scripts')
</div>
</body>
</html>