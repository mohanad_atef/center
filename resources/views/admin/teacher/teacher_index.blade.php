<!DOCTYPE html>
<html>
<head>
    @include('includes.admin.header')
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    @include('includes.admin.main-header')
    @include('includes.admin.main-sidebar')
    <div class="content-wrapper">
        @include('includes.admin.error')
        <br>
                <div class="row">
                    <div class="col-md-11">
                        <h1 align="center">Teacher</h1>
                    </div>
                    @permission('teacher-create')
                    <div style="margin-left: 35px">
                        <a href="{{  url('/admin/teacher/create') }}" style="font-size: 16px" class="btn btn-sm btn-primary">Add</a>
                    </div>
                    <br>
                    @endpermission
                </div>
            <div class="row">
                <div class="col-md-12">
                    @if(count($teacher) > 0)
                        <div align="center" class="col-md-12 table-responsive">
                            <table id="dynamic-table" class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th class="center">#</th>
                                    <th class="center">Name</th>
                                    <th class="center">Email</th>
                                    <th class="center">mobile</th>
                                    <th class="center">Control</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $count=1; ?>
                                @foreach($teacher as $myteacher)
                                    <tr>
                                        <td>{{ $count++ }}</td>
                                        <td class="center">{{ $myteacher->user->name }}</td>
                                        <td class="center">{{ $myteacher->user->email }}</td>
                                        <td class="center"><a href=tel:'{{ $myteacher->mobile }}'>{{ $myteacher->mobile }}</a></td>
                                        <td class="center">
                                            @permission('teacher-status')
                                            @if($myteacher->user->statues ==1)
                                                <h7>Active</h7><a href="{{ url('/admin/teacher/statues/'.$myteacher->user->id)}}"><i
                                                            class="ace-icon fa fa-close"></i></a>
                                            @elseif($myteacher->user->statues ==0)
                                                <h7>Dactive</h7><a href="{{ url('/admin/teacher/statues/'.$myteacher->user->id)}}"><i
                                                            class="ace-icon fa fa-check-circle"></i></a>
                                            @endif
                                            @endpermission
                                            @permission('teacher-edit')
                                            <a href="{{ url('/admin/teacher/edit/'.$myteacher->user->id)}}"><i class="ace-icon fa fa-edit bigger-120  edit" data-id="">edit</i></a>
                                            @endpermission
                                            @if(Auth::user()->id != $myteacher->user->id)
                                                @permission('teacher-password')
                                                <a href="{{url('admin/teacher/reset/'.$myteacher->user->id)}}" class="btn btn-success">reset password</a>
                                                @endpermission
                                                @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="empty" align="center">There is no Teacher to show</div>
                    @endif
                </div>
            </div>
        </div>
    @include('includes.admin.footer')
    @include('includes.admin.scripts')
</div>
</body>
</html>