<!DOCTYPE html>
<html>
<head>
    @include('includes.admin.header')
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    @include('includes.admin.main-header')
    @include('includes.admin.main-sidebar')
    <div class="content-wrapper">
        @include('includes.admin.error')
        <div class="col-md-12">
            <div align="center">{{ __('Edit Student') }}</div>
            <form action="{{url('admin/student/edit/'.$student->user->id)}}" style="margin-right: 10px;margin-left: 10px;"  method="POST">
                {{csrf_field()}}
                <div class="form-group{{ $errors->has('name') ? ' has-error' : "" }}">
                    name : <input type="text"  class="form-control" name="name" value="{{$student->user->name}}" placeholder="enter you name">
                </div>
                <div class="form-group{{ $errors->has('email') ? ' has-error' : "" }}">
                    email : <input type="email" class="form-control" name="email" value="{{$student->user->email}}" placeholder="enter you email">
                </div>
                <div class="form-group{{ $errors->has('mobile') ? ' has-error' : "" }}">
                    mobile : <input type="text" value="{{$student->mobile}}" class="form-control" name="mobile" placeholder="Enter You mobile">
                </div>
                <div align="center">
                <div class="form-group">
                   center :  <select id="center_id" type="center_id" class="form-control" name="center_id"  >
                        @foreach($center as $key => $mycenter)
                            <option value="{{$key}}"  @if($student->center_id == $key)){ selected  } @else{   }@endif  > {{$mycenter}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    year :  <select id="year_id" type="year_id" class="form-control" name="year_id"  >
                        @foreach($year as $key => $myyear)
                            <option value="{{$key}}"  @if($student->year_id == $key)){ selected  } @else{   }@endif  > {{$myyear}}</option>
                        @endforeach
                    </select>
                </div>
                    <input type="submit" style="font-size: 16px" class="btn btn-primary" value="Done">
                </div>
            </form>
        </div>
    </div>
    @include('includes.admin.footer')
    @include('includes.admin.scripts')
</div>
</body>
</html>