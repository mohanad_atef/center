<!DOCTYPE html>
<html>
<head>
    @include('includes.admin.header')
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    @include('includes.admin.main-header')
    @include('includes.admin.main-sidebar')
    <div class="content-wrapper">
        @include('includes.admin.error')
        <br>
                <div class="row">
                    <div class="col-md-11">
                        <h1 align="center">Student</h1>
                    </div>
                    @permission('student-create')
                    <div style="margin-left: 35px">
                        <a href="{{  url('/admin/student/create') }}" style="font-size: 16px" class="btn btn-sm btn-primary">Add</a>
                    </div>
                    <br>
                    @endpermission
                </div>
            <div class="row">
                <div class="col-md-12">
                    @if(count($student) > 0)
                        <div align="center" class="col-md-12 table-responsive">
                            <table id="dynamic-table" class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th class="center">#</th>
                                    <th class="center">Name</th>
                                    <th class="center">Email</th>
                                    <th class="center">mobile</th>
                                    <th class="center">center</th>
                                    <th class="center">year</th>
                                    <th class="center">Control</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $count=1; ?>
                                @foreach($student as $mystudent)
                                    <tr>
                                        <td>{{ $count++ }}</td>
                                        <td class="center">{{ $mystudent->user->name }}</td>
                                        <td class="center">{{ $mystudent->user->email }}</td>
                                        <td class="center"><a href=tel:'{{ $mystudent->mobile }}'>{{ $mystudent->mobile }}</a></td>
                                        <td class="center">{{ $mystudent->center->name }}</td>
                                        <td class="center">{{ $mystudent->year->name }}</td>
                                        <td class="center">
                                            @permission('student-status')
                                            @if($mystudent->user->statues ==1)
                                                <h7>Active</h7><a href="{{ url('/admin/student/statues/'.$mystudent->user->id)}}"><i
                                                            class="ace-icon fa fa-close"></i></a>
                                            @elseif($mystudent->user->statues ==0)
                                                <h7>Dactive</h7><a href="{{ url('/admin/student/statues/'.$mystudent->user->id)}}"><i
                                                            class="ace-icon fa fa-check-circle"></i></a>
                                            @endif
                                            @endpermission
                                            @permission('student-edit')
                                            <a href="{{ url('/admin/student/edit/'.$mystudent->user->id)}}"><i class="ace-icon fa fa-edit bigger-120  edit" data-id="">edit</i></a>
                                            @endpermission
                                                @permission('student-password')
                                                <a href="{{url('admin/student/reset/'.$mystudent->user->id)}}" class="btn btn-success">reset password</a>
                                                @endpermission
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="empty" align="center">There is no Student to show</div>
                    @endif
                </div>
            </div>
        </div>
    @include('includes.admin.footer')
    @include('includes.admin.scripts')
</div>
</body>
</html>