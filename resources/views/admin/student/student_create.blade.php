<!DOCTYPE html>
<html>
<head>
    @include('includes.admin.header')
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    @include('includes.admin.main-header')
    @include('includes.admin.main-sidebar')
    <div class="content-wrapper">
        @include('includes.admin.error')
        <div class="col-md-12">
            <br>
            <div align="center"><h3>{{ __('Add Student') }}</h3></div>
            <form action="{{url('admin/student/create')}}" method="POST" style="margin-right: 10px;margin-left:10px ">
                {{csrf_field()}}
                <div class="form-group{{ $errors->has('name') ? ' has-error' : "" }}">
                    Name : <input type="text" value="{{Request::old('name')}}" class="form-control" name="name"
                                  placeholder="Enter You name">
                </div>
                <div class="form-group{{ $errors->has('email') ? ' has-error' : "" }}">
                    email : <input type="text" value="{{Request::old('email')}}" class="form-control"
                                   name="email" placeholder="Enter You email">
                </div>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : "" }}">
                    password : <input type="password" value="{{Request::old('password')}}" class="form-control"
                                      name="password" placeholder="Enter You password">
                </div>
                <div class="form-group">
                    password confirmation : <input type="password" value="{{Request::old('password')}}"
                                                   class="form-control" name="password_confirmation"
                                                   placeholder="Enter You password">
                </div>
                <div class="form-group{{ $errors->has('mobile') ? ' has-error' : "" }}">
                    mobile : <input type="text" value="{{Request::old('mobile')}}" class="form-control" name="mobile" placeholder="Enter You mobile">
                </div>
                <div align="center">
                <div class="form-group">
                   center : <select id="center_id" name="center_id" type="center_id" class="form-control"  >
                        <option value="" selected disabled>Select</option>
                        @foreach($center as $key => $mycenter)
                            <option value="{{$key}}"> {{$mycenter}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    year : <select id="year_id" name="year_id" type="year_id" class="form-control"  >
                        <option value="" selected disabled>Select</option>
                        @foreach($year as $key => $myyear)
                            <option value="{{$key}}"> {{$myyear}}</option>
                        @endforeach
                    </select>
                </div>
                <input type="submit" class="btn btn-primary" style="font-size: 16px" value="Done ">
                </div>
                <br>
            </form>
        </div>
    </div>
    @include('includes.admin.footer')
    @include('includes.admin.scripts')
</div>
</body>
</html>