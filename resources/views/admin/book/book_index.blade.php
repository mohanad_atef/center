<!DOCTYPE html>
<html>
<head>
    @include('includes.admin.header')
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    @include('includes.admin.main-header')
    @include('includes.admin.main-sidebar')
    <div class="content-wrapper">
        <br>
        @include('includes.admin.error')

        <div class="page-content">
            <div class="page-header">
                <div class="row">
                    <div class="col-md-11">
                        <h1 align="center">Book</h1>
                    </div>
                    @permission('book-create')
                    <div style="margin-left: 35px">
                        <a href="{{  url('/admin/book/create') }}" style="font-size:16px;" class="btn btn-sm btn-primary">Add</a>
                    </div>
                    @endpermission
                </div>
            </div>
            <div align="center">
            <div class="row">
                <div class="col-md-12">
                    @if(count($book) > 0)
                        <div align="center" class="col-md-12 table-responsive">
                            <table id="dynamic-table" class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th class="center">#</th>
                                    <th class="center">title</th>
                                    <th class="center">description</th>
                                    @permission('control')
                                    <th class="center">center</th>
                                    <th class="center">year</th>
                                    @endpermission
                                    <th class="center">pdf</th>
                                    @permission('control')
                                    <th class="center">Control</th>
                                    @endpermission
                                </tr>
                                </thead>
                                <tbody>
                                <?php $count=1; ?>
                                @foreach($book as $mybook)
                                    @if($role->role_id =='2')
                                    @foreach($mybook->center as $book_center)
                                    @if($book_center->id == $student->center_id)
                                        <tr>
                                        <td>{{ $count++ }}</td>
                                        <td class="center">{{ $mybook->title }}</td>
                                        <td class="center">{!! $mybook->description !!}</td>
                                            @permission('control')
                                        <td class="center">
                                            @foreach($mybook->center as $book_center)
                                                [{{ $book_center->name }}],
                                            @endforeach</td>
                                        <td class="center">
                                                {{ $mybook->year->name }}
                                        </td>
                                            @endpermission
                                        <td class="center">
                                            <a aria-label="{{trans('lang.media')}}"
                                               href="{{ asset('public/files/pdf/'.$mybook->pdf) }}">{{ $mybook->pdf }}</a>
                                        </td>
                                        @permission('control')
                                        <td class="center">
                                            @permission('book-edit')
                                            <a href="{{ url('/admin/book/edit/'.$mybook->id)}}"><i class="ace-icon fa fa-edit bigger-120  edit" data-id="">edit</i></a>
                                            @endpermission
                                            @permission('book-delete')
                                            <a href="{{url('admin/book/delete/'.$mybook->id)}}" onclick="return confirm('Are you sure?')" style="color: red"> <i class="ace-icon fa fa-trash bigger-120 ">delete</i></a>
                                            @endpermission
                                        </td>
                                        @endpermission
                                    </tr>
                                        @endif
                                    @endforeach
                                        @else
                                        <tr>
                                            <td>{{ $count++ }}</td>
                                            <td class="center">{{ $mybook->title }}</td>
                                            <td class="center">{!! $mybook->description !!}</td>
                                            <td class="center">
                                                @foreach($mybook->center as $book_center)
                                                    [{{ $book_center->name }}],
                                                @endforeach</td>
                                            <td class="center">
                                                {{ $mybook->year->name }}
                                            </td>
                                            <td class="center">
                                                <a aria-label="{{trans('lang.media')}}"
                                                   href="{{ asset('public/files/pdf/'.$mybook->pdf) }}">{{ $mybook->pdf }}</a>
                                            </td>
                                            @permission('control')
                                            <td class="center">
                                                @permission('book-edit')
                                                <a href="{{ url('/admin/book/edit/'.$mybook->id)}}"><i class="ace-icon fa fa-edit bigger-120  edit" data-id="">edit</i></a>
                                                @endpermission
                                                @permission('book-delete')
                                                <a href="{{url('admin/book/delete/'.$mybook->id)}}" onclick="return confirm('Are you sure?')" style="color: red"> <i class="ace-icon fa fa-trash bigger-120 ">delete</i></a>
                                                @endpermission
                                            </td>
                                            @endpermission
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="empty" align="center">There is no Book to show</div>
                    @endif
                </div>
            </div>
        </div>
        </div>
    </div>
    @include('includes.admin.footer')
    @include('includes.admin.scripts')
</div>
</body>
</html>