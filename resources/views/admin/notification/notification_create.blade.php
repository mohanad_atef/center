<!DOCTYPE html>
<html>
<head>
    @include('includes.admin.header')
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{url('public/css/admin/multi-select.css')}}">
    <style>

        .ms-container {
            width: 70%;
        }

        li.ms-elem-selectable, .ms-selected {
            padding: 5px !important;
        }

        .ms-list {
            height: 310px !important;
        }
    </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    @include('includes.admin.main-header')
    @include('includes.admin.main-sidebar')
    <div class="content-wrapper">
        @include('includes.admin.error')
        <br>
        <div align="center"><h3>{{ __('Notification') }}</h3></div>
        <form action="{{url('admin/notification/create')}}" enctype="multipart/form-data" method="POST"
              style="margin-right: 10px;margin-left:10px ">
            {{csrf_field()}}
            <div class="form-group{{ $errors->has('description') ? ' has-error' : "" }}">
                description : <textarea type="text" value="{{Request::old('description')}}" class="form-control"
                                        name="description"></textarea>
            </div>
            <div class="form-group{{ $errors->has('order') ? ' has-error' : "" }}">
                order : <input type="text" value="{{Request::old('order')}}" class="form-control"
                               name="order" placeholder="Enter You order">
            </div>

            <div class="form-group">
                year : <select id="year_id" name="year_id" type="year_id" class="form-control">
                    <option value="" selected disabled>Select</option>
                    @foreach($year as $key => $myyear)
                        <option value="{{$key}}"> {{$myyear}}</option>
                    @endforeach
                </select>
            </div>
            <div align="center">
                <div class="form-group" style="margin-left:450px; ">
                    choose Center :
                </div>
                <div class="form-group">
                    <select id="center_id" multiple='multiple' name="center[]">
                        @foreach($center as  $mycenter)
                            <option value="{{$mycenter->id}}"> {{$mycenter->name}}</option>
                        @endforeach
                    </select>
                </div>
                <input type="submit" class="btn btn-primary" style="font-size: 18px" value="Done ">
            </div>
            <br>
        </form>

    </div>
    @include('includes.admin.footer')
    @include('includes.admin.scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
    <!-- Bootstrap JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/js/bootstrap.min.js"></script>
    <script src="{{url('public/js/admin/jquery.multi-select.js')}}"></script>
    <script>
        $('#center_id').multiSelect();
    </script>
</div>
</body>
</html>