<html>
<head>
    @include('includes.admin.header')
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{url('public/css/admin/multi-select.css')}}">
    <style>

        .ms-container {
            width: 70%;
        }

        li.ms-elem-selectable, .ms-selected {
            padding: 5px !important;
        }

        .ms-list {
            height: 310px !important;
        }
    </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    @include('includes.admin.main-header')
    @include('includes.admin.main-sidebar')
    <div class="content-wrapper">
        @include('includes.admin.error')
        <br>
        <div align="center"><h3>{{ __('Notification') }}</h3></div>
        <form action="{{url('admin/notification/edit/'.$notification->id)}}" method="POST"
              style="margin-right: 10px;margin-left: 10px;">
            {{csrf_field()}}
            <div class="form-group{{ $errors->has('description') ? ' has-error' : "" }}">
                description : <textarea type="text" id="description" class="form-control" name="description"
                                        placeholder="enter you description">{{$notification->description}}</textarea>
            </div>
            <div class="form-group{{ $errors->has('order') ? ' has-error' : "" }}">
                order : <input type="text" value="{{$notification->order}}" class="form-control" name="order"
                               placeholder="Enter You order">
            </div>
            <div class="form-group">
                year : <select id="year_id" type="year_id" class="form-control" name="year_id">
                    @foreach($year as $key => $myyear)
                        <option value="{{$key}}" @if($notification->year_id == $key)){ selected } @else{
                                }@endif > {{$myyear}}</option>
                    @endforeach
                </select>
            </div>
            <div align="center">
                choose Center :
                <div class="form-group">
                    <select id="center_id" multiple='multiple' name="center[]">
                        @foreach($center as  $mycenter)
                            <option value="{{$mycenter->id}}"
                                    @foreach($notification_center as  $mycenter_notification) @if($mycenter_notification->center_id ==$mycenter->id)){
                                    selected } @else{ }@endif @endforeach > {{$mycenter->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div align="center">
                <input type="submit" class="btn btn-primary" style="font-size: 18px" value="Done">
            </div>
            <br>
        </form>

    </div>
    @include('includes.admin.footer')
    @include('includes.admin.scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
    <!-- Bootstrap JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/js/bootstrap.min.js"></script>
    <script src="{{url('public/js/admin/jquery.multi-select.js')}}"></script>
    <script>
        $('#center_id').multiSelect();
    </script>
</div>
</body>
</html>