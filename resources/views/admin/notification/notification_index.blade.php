<!DOCTYPE html>
<html>
<head>
    @include('includes.admin.header')
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    @include('includes.admin.main-header')
    @include('includes.admin.main-sidebar')
    <div class="content-wrapper">
        <br>
        @include('includes.admin.error')
        <div class="page-content">
            <div class="page-header">
                <div class="row">
                    <div class="col-md-11">
                        <h1 align="center">Notification</h1>
                    </div>
                    @permission('notification-create')
                    <div style="margin-left: 35px">
                        <a href="{{  url('/admin/notification/create') }}" style="font-size:16px;" class="btn btn-sm btn-primary">Add</a>
                    </div>
                    @endpermission
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    @if(count($notification) > 0)
                        <div align="center" class="col-md-12 table-responsive">
                            <table id="dynamic-table" class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th class="center">#</th>
                                    <th class="center">description</th>
                                    <th class="center">center</th>
                                    <th class="center">year</th>
                                    @permission('control')
                                    <th class="center">Control</th>
                                    @endpermission
                                </tr>
                                </thead>
                                <tbody>
                                <?php $count=1; ?>
                                @foreach($notification as $mynotification)
                                        <tr>
                                            <td>{{ $count++ }}</td>
                                            <td class="center">{!! $mynotification->description !!}</td>
                                            <td class="center">
                                                @foreach($mynotification->center as $notification_center)
                                                    [{{ $notification_center->name }}],
                                                @endforeach</td>
                                            <td class="center">
                                                {{ $mynotification->year->name }}
                                            </td>
                                            @permission('control')
                                            <td class="center">
                                                @permission('notification-edit')
                                                <a href="{{ url('/admin/notification/edit/'.$mynotification->id)}}"><i class="ace-icon fa fa-edit bigger-120  edit" data-id="">edit</i></a>
                                                @endpermission
                                                @permission('notification-delete')
                                                <a href="{{url('admin/notification/delete/'.$mynotification->id)}}" onclick="return confirm('Are you sure?')" style="color: red"> <i class="ace-icon fa fa-trash bigger-120 ">delete</i></a>
                                                @endpermission
                                            </td>
                                            @endpermission
                                        </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="empty" align="center">There is no Notification to show</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @include('includes.admin.footer')
    @include('includes.admin.scripts')
</div>
</body>
</html>