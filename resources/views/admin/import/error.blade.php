<!DOCTYPE html>
<html>
<head>
    @include('includes.admin.header')
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    @include('includes.admin.main-header')
    @include('includes.admin.main-sidebar')
    <div class="content-wrapper">
        @include('includes.admin.error')
        <br>
        <div class="page-content">
            <div class="page-header">
                <div class="row">
                    <div class="col-md-11">
                        <h1 align="center">import sheet</h1>
                    </div>
                    <div class="col-md-1">
                        <a href="{{  url('/admin/import') }}" class="btn btn-sm btn-primary">import</a>
                        <a href="{{  url('/admin/import/error') }}" class="btn btn-sm btn-primary">test</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div align="center" class="col-md-12 table-responsive">
                        <table id="dynamic-table" class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th align="center">center</th>
                                <th align="center">year</th>
                                <th align="center">email in system</th>
                                <th align="center">email in sheet</th>
                                </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="center">
                                    @foreach($center as $mycenter)
                                        <li>{{ $mycenter}}</li>
                                    @endforeach
                                </td>
                                <td class="center">
                                    @foreach($year as $myyear)
                                        <li>{{ $myyear}}</li>
                                    @endforeach
                                </td>
                                <td class="center">
                                    @foreach($email as $myemail)
                                        <li>{{ $myemail}}</li>
                                    @endforeach
                                </td>
                                <td class="center">
                                    @foreach($emailtemplet1 as $myemail)
                                        <li>{{ $myemail}}</li>
                                    @endforeach
                                        @foreach($emailtemplet11 as $myemail)
                                            <li>{{ $myemail}}</li>
                                        @endforeach
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('includes.admin.footer')
    @include('includes.admin.scripts')
</div>
</body>
</html>