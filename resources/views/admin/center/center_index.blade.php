<!DOCTYPE html>
<html>
<head>
    @include('includes.admin.header')
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    @include('includes.admin.main-header')
    @include('includes.admin.main-sidebar')
    <div class="content-wrapper">
        @include('includes.admin.error')
        <br>
                <div class="row">
                    <div class="col-md-11">
                        <h1 align="center">Center</h1>
                    </div>
                    @permission('center-create')
                    <div  style="margin-left:35px;">
                        <a href="{{  url('/admin/center/create') }}" style=" font-size: 16px;" class="btn btn-sm btn-primary">Add</a>
                    <br><br>
                    </div>
                    @endpermission
                </div>
            <div class="row">
                <div class="col-md-12">
                    @if(count($center) > 0)
                        <div align="center" class="col-md-12 table-responsive">
                            <table id="dynamic-table" class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th class="center">#</th>
                                    <th class="center">Name</th>
                                    <th class="center">Control</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $count=1; ?>
                                @foreach($center as $mycenter)
                                    <tr>
                                        <td>{{ $count++ }}</td>
                                        <td class="center">{{ $mycenter->name }}</td>
                                        <td class="center">
                                            @permission('center-edit')
                                            <a href="{{ url('/admin/center/edit/'.$mycenter->id)}}"><i class="ace-icon fa fa-edit bigger-120  edit" data-id="">edit</i></a>
                                            @endpermission
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="empty" align="center">There is no Center to show</div>
                    @endif
                </div>
            </div>
    </div>
    @include('includes.admin.footer')
    @include('includes.admin.scripts')
</div>
</body>
</html>