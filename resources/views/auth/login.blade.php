@extends('layouts.app')

@section('content')
    @include('includes.admin.error')
    <br><br><br><br><br>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    {{--<div align="center">
                        <img  src="{{ asset('public/images/loge.webp') }}" alt="logo" />
                    </div>--}}

                    <div class="card-body">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                            <p>
                            <h1 align="center">سلسله الوليد</h1></p>
                            <div class="form-group row">

                                <label for="email"
                                       class="col-md-4 col-form-label text-md-right">{{ __('البريد الاكتروني') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email"
                                           class="form-control @error('email') is-invalid @enderror" name="email"
                                           value="{{ old('email') }}" required autocomplete="email" autofocus>

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password"
                                       class="col-md-4 col-form-label text-md-right">{{ __('كلمه السر') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password"
                                           class="form-control @error('password') is-invalid @enderror" name="password"
                                           required autocomplete="current-password">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>


                            <div align="center">
                                <div align="center">
                                    <button type="submit" class="btn btn-danger">
                                        {{ __('Login') }}
                                    </button>
                                    <br>
                                    <br><br>
                                    <div class="copyright">COPYRIGHTS © 2019 | ALL RIGHTS RESERVED TO Mohanad Atef</div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
